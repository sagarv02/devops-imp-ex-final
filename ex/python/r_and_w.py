import csv

# Define the file name
csv_file = "emp_data.csv"

# Read the existing data from the CSV file
existing_data = []
with open(csv_file, mode="r") as file:
    csv_reader = csv.reader(file)
    headers = next(csv_reader)  # Skip the header row
    for row in csv_reader:
        existing_data.append(row)

# Get input for the new employee record
emp_id = input("Enter Employee ID: ")
emp_name = input("Enter Employee Name: ")
dob = input("Enter Date of Birth (dd-mm-yyyy): ")
doj = input("Enter Date of Joining (dd-mm-yyyy): ")

# Create a new employee record
new_employee = [emp_id, emp_name, dob, doj]

# Append the new employee record to the existing data
existing_data.append(new_employee)

# Write the updated data back to the CSV file
with open(csv_file, mode="w", newline="") as file:
    csv_writer = csv.writer(file)
    csv_writer.writerow(headers)
    csv_writer.writerows(existing_data)

print("New employee record added successfully.")

