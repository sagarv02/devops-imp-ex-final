import csv

# Function to read the CSV file and display its content
def read_csv_file(file_name):
    with open(file_name, mode='r', newline='') as file:
        reader = csv.DictReader(file)
        print("Current Employee Data:")
        for row in reader:
            print(f"Employee ID: {row['emp_id']}, Employee Name: {row['emp_name']}, DOB: {row['dob']}, DOJ: {row['doj']}")

# Function to add a new employee record to the CSV file
def add_employee_record(file_name):
    emp_id = input("Enter Employee ID: ")
    emp_name = input("Enter Employee Name: ")
    dob = input("Enter Date of Birth (DD-MM-YYYY): ")
    doj = input("Enter Date of Joining (DD-MM-YYYY): ")

    with open(file_name, mode='a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([emp_id, emp_name, dob, doj])
    
    print("Employee record added successfully!")

# File name
file_name = "emp_data.csv"

# Create the CSV file with initial data if it doesn't exist
with open(file_name, mode='w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["emp_id", "emp_name", "dob", "doj"])
    writer.writerow([10, "John", "18-02-2000", "18-02-2022"])

# Read and display the current employee data
read_csv_file(file_name)

# Add a new employee record
add_employee_record(file_name)

# Read and display the updated employee data
read_csv_file(file_name)
