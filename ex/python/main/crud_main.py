import csv

# Initial data
data = [
    ["emp_id", "emp_name", "dob", "doj"],
    [10, "John", "18-02-2000", "18-02-2022"]
]

# Write the initial data to the CSV file
with open("emp_data.csv", mode="w", newline="") as file:
    writer = csv.writer(file)
    writer.writerows(data)

print("Initial data has been written to emp_data.csv.")




                                                                    # crud operations


def read_csv_file():
    with open("emp_data.csv", mode="r") as file:
        reader = csv.reader(file)
        header = next(reader)  # Skip the header row
        data = [row for row in reader]
        
        
    return header, data

def write_csv_file(header, data):
    with open("emp_data.csv", mode="w", newline="") as file:
        writer = csv.writer(file)
        writer.writerow(header)
        writer.writerows(data)

def create_employee():
    emp_id = input("Enter emp_id: ")
    emp_name = input("Enter emp_name: ")
    dob = input("Enter dob (dd-mm-yyyy): ")
    doj = input("Enter doj (dd-mm-yyyy): ")

    header, data = read_csv_file()
    data.append([emp_id, emp_name, dob, doj])
    write_csv_file(header, data)
    print(f"Employee with emp_id {emp_id} has been added.")

def read_employee():
    header, data = read_csv_file()
    for row in data:
        print(f"emp_id: {row[0]}, emp_name: {row[1]}, dob: {row[2]}, doj: {row[3]}")

def update_employee():
    emp_id = input("Enter the emp_id to update: ")
    header, data = read_csv_file()
    for row in data:
        if row[0] == emp_id:
            row[1] = input("Enter updated emp_name: ")
            row[2] = input("Enter updated dob (dd-mm-yyyy): ")
            row[3] = input("Enter updated doj (dd-mm-yyyy): ")
            write_csv_file(header, data)
            print(f"Employee with emp_id {emp_id} has been updated.")
            return
    print(f"Employee with emp_id {emp_id} not found.")

def delete_employee():
    emp_id = input("Enter the emp_id to delete: ")
    header, data = read_csv_file()
    for row in data:
        if row[0] == emp_id:
            data.remove(row)
            write_csv_file(header, data)
            print(f"Employee with emp_id {emp_id} has been deleted.")
            return
    print(f"Employee with emp_id {emp_id} not found.")

while True:
    print("\nMenu:")


    
    


    print("1. Create Employee")
    print("2. Read Employees")
    print("3. Update Employee")
    print("4. Delete Employee")
    print("5. Exit")
    choice = input("Enter your choice: ")

    if choice == "1":
        create_employee()
    elif choice == "2":
        read_employee()
    elif choice == "3":
        update_employee()
    elif choice == "4":
        delete_employee()
    elif choice == "5":
        break
    else:
        print("Invalid choice. Please select a valid option.")
